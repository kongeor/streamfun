package io.github.kongeor.streamfun;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Tag("fast")
public class FrequenciesTest {

    private Frequencies frequencies = new Frequencies();

    @Test
    @DisplayName("Frequencies Test")
    public void testFreq() {
	Map<Integer, Integer> f = this.frequencies.frequencies(Arrays.asList(1, 2, 2, 3, 1));
	assertEquals(Integer.valueOf(2), f.get(1), "We expect 2 ones");
	assertEquals(Integer.valueOf(2), f.get(2), "We expect 2 twos");
	assertEquals(Integer.valueOf(1), f.get(3), "We expect 1 three");
    }
}