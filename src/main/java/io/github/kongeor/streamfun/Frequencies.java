package io.github.kongeor.streamfun;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Frequencies {

    public <T> Map<T, Integer> frequencies(Iterable<T> iter) {
        return StreamSupport.stream(iter.spliterator(), true)
	    .collect(Collectors.groupingBy(Function.identity()))
	    .entrySet()
	    .stream()
	    .collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue().size()));

    }
}
